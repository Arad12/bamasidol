﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bmasidol;

namespace part1
{
    class Program
    {

        public static void new_array_singers(ref Singer[] singer_array, string nickname1, string song_name)
        {
            /* this function make the array bigger and push a singer to the last sit */
            Array.Resize<Singer>(ref singer_array, singer_array.Length + 1);
            Singer S = new Singer(nickname1);
            S.set_song_name(song_name);
            singer_array[singer_array.Length - 1] = S;
        }


        public static void shufle_the_array(Singer[] singers)
        {
            var rand = new Random();
            // this function shuffle the array
            for (int t = 0; t < singers.Length; t++)
            {
                Singer tmp = singers[t];
                int r = rand.Next(0, singers.Length);
                singers[t] = singers[r];
                singers[r] = tmp;
            }
        }

        public static Singer battle(Singer first_singer, Singer sec_singer, string first_song, string sec_song)
        {
            // this function determines who is the loser, draw is not an option :)
            Console.WriteLine("{0} sing the song: {1} \n ", first_singer.get_nickname(), first_singer.get_current_song());
            Console.WriteLine("\n\n\n\n Enter a grade to: {0}", first_singer.get_nickname());
            int first = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} sing the song: {1} \n ", sec_singer.get_nickname(), sec_singer.get_current_song());
            Console.WriteLine("\n\n\n\n Enter a grade to: {0}", sec_singer.get_nickname());
            int sec = int.Parse(Console.ReadLine());
            if (first > sec)
                return sec_singer;
            else if (first < sec)
                return first_singer;
            else
                Console.WriteLine("some of them must be the winner!");
            return battle(first_singer, sec_singer, first_song, sec_song);
        }

        public static void kick_loser(ref Singer[] singers_array)
        {
            // this function kick the loser singer from the array's singers
            Singer first = singers_array[singers_array.Length - 2];
            Singer sec = singers_array[singers_array.Length - 1];
            Singer loser = battle(first, sec, first.get_current_song(), sec.get_current_song());
            if (loser == first)
            {
                singers_array[singers_array.Length - 2] = sec;
                Array.Resize<Singer>(ref singers_array, singers_array.Length - 1);
            }
            else
                Array.Resize<Singer>(ref singers_array, singers_array.Length - 1);
        }


        static void Main(string[] args)
        {
            Singer[] singers1 = new Singer[0];
            string answer = "yes";
            while (answer == "yes")
            {
                // as long as the answer is 'yes' the user will be ask to enter two new participents and for each' a song.
                Console.WriteLine("******allert***** \n you have to enter two participents each time");
                Console.WriteLine("insert the singer's nickname");
                string nickname = Console.ReadLine();
                Console.WriteLine("insert the singer's song");
                string song = Console.ReadLine();
                new_array_singers(ref singers1, nickname, song);
                Console.WriteLine("insert the singer's nickname");
                string nickname2 = Console.ReadLine();
                Console.WriteLine("insert the singer's song");
                string song2 = Console.ReadLine();
                new_array_singers(ref singers1, nickname2, song2);
                Console.WriteLine("would you like to add another two participents? (yes/no)");
                answer = Console.ReadLine().ToLower();
            }
            // here we are shuffle the array
            shufle_the_array(singers1);

            // kick singers until it's find the winner
            while (singers1.Length > 1)
            {
                kick_loser(ref singers1);
            }


            Console.WriteLine("\n\n\n\n\n\n the winner is: {0}", singers1[0].get_nickname());
        }
    }





}

namespace Bmasidol
{
    public class Singer
    {
        string nickname;
        string song;
        public Singer(string name)
        {
            this.nickname = name;
        }

        public string get_nickname()
        {
            return this.nickname;
        }

        public void set_song_name(string song_name)
        {
            this.song = song_name;
        }

        public string get_current_song()
        {
            return this.song;
        }
    }

    public class Song
    {
        string song_name;
        string song_lyrics;

        public Song(string name, string lyrics)
        {
            this.song_name = name;
            this.song_lyrics = lyrics;
        }

        public void set_name(string name)
        {
            this.song_name = name;
        }

        public string get_song_name()
        {
            return this.song_name;
        }

        public void set_lyrics(string ly)
        {
            this.song_lyrics = ly;
        }

        public string get_lyrics()
        {
            return this.song_lyrics;
        }
    }

    public class Judge
    {
        string fname, lname;
        public Judge(string fname, string lname)
        {
            this.fname = fname;
            this.lname = lname;
        }

        public string Judge_full_name()
        {
            return this.fname + " " + this.lname;
        }

        public int Judgement(Singer singer, string ly)
        {
            Console.WriteLine("enter a grade");
            int grade = int.Parse(Console.ReadLine());
            return grade;
        }


    }
}

